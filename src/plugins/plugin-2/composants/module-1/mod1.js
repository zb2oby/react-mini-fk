import React from 'react';
import pluginRegistry from "../../../../core/PluginRegistry";
import {Link} from 'react-router-dom';
import {Icon} from 'antd';

export default class mod1 extends React.Component {

    render() {

        const Exemple = pluginRegistry.getDependencieComponent(this.props.plugin.key, this.props.module.key, "Example");

        return (
            <div>
                <Link to={this.props.plugin.entryPoint.path} ><Icon style={{fontSize: 32, color:"#000"}} type={"backward"}/>Retour</Link>
                <h3 style={{textAlign: "center"}}>{this.props.module.title}</h3>

                <div style={{textAlign: "center", marginTop: 40}}>
                    ----- Dépendance -----
                    <Exemple/>
                    ----- Dépendance -----
                </div>



                <ul className={"example-description"}>
                    <li>Ce module récupere la valeur du titre depuis la configuration du module executé <span className={"example-span"}>(this.props.module.title)</span></li>
                    <li>Il contiens et affiche une dépendance au module example du plugin 1 <span className={"example-span"}>(pluginRegistry.getDependencieComponent(this.props.plugin.key, this.props.module.key, "Example"))</span></li>
                    <li>Le bouton retour est crée dynamiquement depuis la configuration du plugin executé <span className={"example-span"}>(this.props.plugin.entryPoint.path)</span></li>
                </ul>

            </div>
        )
    }
}