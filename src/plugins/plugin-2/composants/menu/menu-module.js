import React from 'react';
import SubMenuPlugin from "../../../../core/composants/layouts/pluginsLayouts/SubMenuPlugin";
import pluginRegistry from "../../../../core/PluginRegistry";
import Loading from "../../../../core/composants/layouts/basics/Loading";



export default class MenuModule extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            ability: null,
            externalMods: pluginRegistry.getConfiguration("Plugin1")
        }

    }

    render() {

        const stateForLinks = {
            someState: "someState",
        };

        const modulesWithState = this.props.plugin.mods.map((module) => {
            return {...module, state: stateForLinks}
        });
        const externalModulesWithState = this.state.externalMods && this.state.externalMods.mods.map((module) => {
            return {...module, state: stateForLinks}
        });


        if (this.state.loading) return <Loading/>
        return (
            <div className="container-fluide">

                <h3 style={{textAlign:"center"}}>{this.props.plugin.entryPoint.title}</h3>

                <SubMenuPlugin
                    {...this.props}
                    plugin={{...this.props.plugin, mods: modulesWithState}} //override la props plugin de this.props
                    header={false}
                    externalModules={externalModulesWithState}
                />
                <ul className={"example-description"}>
                    <li>Ce menu ne charge pas de header mais récupere la valeur du titre depuis la configuration du plugin executé  <span className={"example-span"}>(this.props.plugin.entryPoint.title)</span></li>
                    <li>Ce menu charge une configuration de plugin externe afin d'afficher aussi les modules d'un plugin externe dans ce sous-menu <span className={"example-span"}>(pluginRegistry.getConfiguration("Plugin1"))</span></li>
                    <li>En revanche on devrait voir apparaitre le module 2 du plugin 2 mais celui-ci etant définit withAuth et notre application n'ayant pas encore d'authentification, celui-ci n'est pas affiché</li>
                </ul>
            </div>
        );


    }
}
