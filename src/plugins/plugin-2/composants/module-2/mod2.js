import React from 'react';
import Unauthorized from "../../../../core/composants/layouts/basics/unauthorized";

export default class mod2 extends React.Component {

    render() {

        const aclAction = this.props.module.accessControlAction;
        const aclEntity = this.props.module.accessControlEntity;

        if (this.props.ability && !this.prop.ability.can(aclAction, aclEntity)) return <Unauthorized/>;

        return (
            <div>
                <h3>Ceci est le module 2 du plugin 2</h3>
            </div>
        )
    }
}