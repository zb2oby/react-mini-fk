import React from 'react';
import HeaderPluginModule from "../../../../core/composants/layouts/pluginsLayouts/HeaderPluginModule";
import Example from "./Example";

export default class Module1 extends React.Component {

    render() {
        return (
            <div>
                <HeaderPluginModule {...this.props}/>
                <div style={{textAlign: "center", marginTop: 40}}>
                    <Example/>
                </div>
                <ul className={"example-description"}>
                    <li>Ce module Charge le header des modules <span className={"example-span"}>HeaderPluginModule.js</span></li>
                    <li>Il inclus le composant Example</li>
                </ul>
            </div>
        )
    }
}