import React from 'react';
import SubMenuPlugin from "../../../../core/composants/layouts/pluginsLayouts/SubMenuPlugin";

export default class MenuPlugin extends React.Component{

    render(){
        return (
            <div>
                <SubMenuPlugin {...this.props} header={true}/>
                <ul className={"example-description"}>
                    <li>Ce menu charge le layout Header des sous-menus <span className={"example-span"}>HeaderPluginEntryPoint.js</span></li>
                </ul>
            </div>

        );
    }
}