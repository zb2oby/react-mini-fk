import React from 'react';
import LateralSelectionPanel from "../../../../core/composants/layouts/complex/LateralSelectionPanel";
import dataSelected from "./dataSelected";

export default class lateralSelExample extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items : [
                {id: 1, title: "item 1", content: "contenu de la donnée 1"},
                {id: 2, title: "item 2", content: "contenu de la donnée 2"},
                {id: 3, title: "item 3", content: "contenu de la donnée 3"}
            ]
        }
    }

    render() {

        return (
            <div>
                <LateralSelectionPanel
                    selectorName={"Sélection"}
                    dataSectionName={"Données séléctionnées"}
                    items={this.state.items}
                    titleProperty={"title"}
                    filterName={"id"}
                    dataComponent={dataSelected}
                />

                <ul className={"example-description"}>
                    <li>Ce module Charge le composant de sélection latérale <span className={"example-span"}>LateralSelectionPanel.js</span></li>
                </ul>
            </div>
        )
    }
}