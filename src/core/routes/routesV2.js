import React from 'react';
import {Switch, Redirect} from 'react-router-dom';
import pluginRegistry from "../PluginRegistry";
import AppliedRoute from "./appliedRoute";

/**
 * Génère les routes en fonction des composants déclarés dans les configurations de chaque plugin
 * @param childProps
 * @returns {*}
 */
export default ({ childProps }) => {

    const coreConf = pluginRegistry.getConfiguration("Core");

    const routablePluginsComponents = [];
    pluginRegistry.modules.map((plugin)=> {

        if (plugin.mods) {
            plugin.mods.map((mod)=> {
                routablePluginsComponents.push(mod)
                return mod;
            })
        }
        if (plugin.entryPoint.component) {
            routablePluginsComponents.push(plugin.entryPoint);
        }
        return plugin
    });

    const routes =
        routablePluginsComponents.map(mod=> (
            <AppliedRoute path={mod.path} exact={mod.exact ? mod.exact : false} component={mod.component}/>
        ));

    const routableCoreComponents = [
        <Redirect to={coreConf.entryPoint.path}/>
    ];

    routableCoreComponents.map((coreRoute) => {
        routes.push(coreRoute)
        return coreRoute
    });


    return  <Switch children={routes} />
}
