import Dispatcher from "./Dispatcher";
import {WithAuth} from "./auth/auth";
import React from "react";
import {getPluginByKey} from "./services/CoreService";


/**
 * Charge les configurations json par defaut des plugins
 * @returns {Array}
 */
function loadPluginsJson() {
    const requireContext = require.context('../plugins', true, /json$/);
    const json = [];
    requireContext.keys().forEach((key) => {
        const obj = requireContext(key);
        json.push(obj);
    });
    return json;
}

/**
 * Charge la configuration principale du core
 * @returns {Array}
 */
function loadCoreJson() {
    const requireContext = require.context('', true, /json$/);
    const json = [];
    requireContext.keys().forEach((key) => {
        const obj = requireContext(key);
        json.push(obj);
    });
    return json;
}

/**
 * Charge les configurations de plugins enregistrées en base et override les configuration par defaut
 * @returns {Promise<any[]>}
 */
async function loadPlugins() {
    let pluginsConf = loadPluginsJson();
    let coreConf = loadCoreJson();
    let jsonConf = pluginsConf.concat(coreConf);

    const promises = await jsonConf.map(async (plugin)=> {
        return await getPluginByKey(plugin.key).then((res)=> {
            if (res) {
                return {...plugin, active: res.active, position: res.position, title: res.title}
            }
            return plugin;
        });
    })

    return Promise.all(promises);
}

/**
 * Permet de charger un composant avec la configuration du plugin dont il provient
 * @param WrappedComponent
 * @param plugin
 * @param module
 * @returns {{new(*=): WithPluginConfiguration, prototype: WithPluginConfiguration}}
 * @constructor
 */
const FromPlugin =
    (WrappedComponent, plugin, module) => {
        return class WithPluginConfiguration extends React.Component {
            render() {
                return (<WrappedComponent plugin={plugin} module={module} {...this.props}/>);
            }
        }
    };

const UnsatisfiedWithMessage = (plugin, mod, pluginKey, message) => {
    return class Unsatisfied extends React.Component {
        render() {
            return (
                <div className={"unsatisfied"}>
                    <h2 >Dépendence au plugin {pluginKey} non satisfaite dans le module {plugin} -> {mod}.</h2>
                    <p>{message}</p>
                    <p>Vérifiez si celui-ci existe ou est bien déclaré dans les dépendances</p>
                </div>);
        }
    }

}

/**
 * Enregistre les plugin dans le registre de plugin (store) et charge les composant declarés dans les conf
 * @returns {Promise<void>}
 */
export function storePlugins(plugins) {
    //tri des configs trouvées par ordre d'apparition definis
    let filteredOrderedPlugins = plugins.filter((p)=> {return p.active === true || p.key === "Core"}).sort((function(a, b){return a.position - b.position}));

    //Chargement des composants (routables) déclarés pour les modules du plugins
    let requirePlugins = filteredOrderedPlugins.map((plugin)=> {
        //chargement des sous-composants du plugin s'il y en a
        if (plugin.mods) {
            let modsWithRequire = plugin.mods.map((mod)=> {

                let component = null;
                let depcomponent = null;
                //Vérification des dépendances
                let dependencies = [];
                if (mod.dependencies) {
                    dependencies = mod.dependencies.map((dep)=> {
                        let message = "";
                        let depPlugin = plugins.find((depPlug) => {return depPlug.key === dep.pluginKey});
                        if (depPlugin !== undefined && dep.moduleKey !== undefined) {
                            let depModule = depPlugin.mods.find((depMod)=> {return depMod.key === dep.moduleKey});
                            if (depModule === undefined) {
                                message = "Module //" + dep.moduleKey + "// inexistant.";
                                component = UnsatisfiedWithMessage(plugin.key, mod.key, dep.pluginKey, message);
                            } else {
                                //chargement du composant de dépendance pour recuperation dans le composant demandeur (au lieu d'un import direct) => PERMET DE GERER LES ERREUR DE DEPENDANCE et d'eviter les imports direct depuis les sources
                                try {
                                    if (plugin.key === "Core") {
                                        depcomponent = FromPlugin(require(`./composants/${depModule.component.split("/")[0]}/${dep.componentName}.js`).default,depPlugin,depModule);
                                    } else {
                                        depcomponent = FromPlugin(require(`../plugins${depPlugin.entryPoint.path}/composants/${depModule.component.split("/")[0]}/${dep.componentName}.js`).default,depPlugin,depModule);
                                    }
                                } catch (error) {
                                    message = "Problème de chargement du composant \"" + dep.componentName + "\" dans ce module.";
                                    component = UnsatisfiedWithMessage(plugin.key, mod.key, dep.pluginKey, message);
                                }
                            }
                        } else {
                            message = "Plugin //" + dep.pluginKey + "// inexistant.";
                            component = UnsatisfiedWithMessage(plugin.key, mod.key, dep.pluginKey, message);
                        }

                        return {...dep, component: depcomponent};

                    });
                }

                //Chargement du composant
                if (component === null) {
                    if (plugin.key === "Core") {
                        component = FromPlugin(require(`./composants/${mod.component}.js`).default, plugin, mod);
                    } else {
                        component = FromPlugin(require(`../plugins${plugin.entryPoint.path}/composants/${mod.component}.js`).default, plugin, mod);
                    }
                    if (mod.withAuth) {
                        component = WithAuth(component);
                    }
                }


                return {...mod, component: component, dependencies: dependencies}


            });
            return {...plugin, mods: modsWithRequire}
        }
        return plugin;
    });

    //Chargement du composant (routable) déclarés pour le menu du plugin
    requirePlugins.map((plugin) => {
        if (plugin.entryPoint.component) {
            let entryPoint = plugin.entryPoint;
            let component;
            if (plugin.key === "Core") {
                component = FromPlugin(require(`./composants/${plugin.entryPoint.component}.js`).default, plugin, plugin.entryPoint);
            } else {
                component = FromPlugin(require(`../plugins${plugin.entryPoint.path}/composants/${plugin.entryPoint.component}.js`).default, plugin, plugin.entryPoint);
            }
            if (plugin.entryPoint.withAuth) {
                component = WithAuth(component);
            }
            if (plugin.key === "Core" && plugin.logo !== undefined) {
                let ext = plugin.logo.split(".")[1];
                let fileName = plugin.logo.split(".")[0];
                plugin.logo = require('../assets/img/' + fileName +'.'+ ext);
            }
            entryPoint.component = component;
            return {...plugin, entryPoint: entryPoint}
        }
        return plugin;

    });
    return requirePlugins

}

/**
 * Enregistre les configuration de plugin en fonction de l'existence ou non de la gestion en base des plugin
 * @returns {Promise<void>}
 */
export async function registerPlugins() {

    let coreConfiguration = loadCoreJson()[0];
    if (!coreConfiguration.externalConfEnabled) {
        let pluginsConf = loadPluginsJson();
        let coreConf = loadCoreJson();
        let plugins = pluginsConf.concat(coreConf);
        Dispatcher.dispatch({
            type: 'UPDATE_REGISTERED_PLUGINS',
            modules: storePlugins(plugins)
        })
    } else {
        await loadPlugins().then((plugins)=>{
            Dispatcher.dispatch({
                type: 'UPDATE_REGISTERED_PLUGINS',
                modules: storePlugins(plugins)
            })
        });
    }

};

