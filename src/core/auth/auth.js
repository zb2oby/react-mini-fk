import React from 'react';
import {Redirect, Switch} from "react-router-dom";
import {defineAbilitiesForUser} from "./ability";
import pluginRegistry from "../PluginRegistry";

// Authenticated middleware
/**
 * Composant qui permet de charger un composant enfant avec les droit de l'utilisateur authentifié
 * @param WrappedComponent le compposant à passer via le filtre
 * @returns {{new(*=): WithAuthentication, prototype: WithAuthentication}}
 * @constructor
 */
export const WithAuth =
    (WrappedComponent) => {
        return class WithAuthentication extends React.Component {

            constructor(props) {
                super(props);
                this.coreAuth = pluginRegistry.getModuleConfiguration("Core", "login");
                this.coreConf = pluginRegistry.getConfiguration("Core");
            }

            render() {


                if (this.coreConf.authEnabled && sessionStorage.getItem('isAuthenticated')) {

                    let ability = defineAbilitiesForUser(JSON.parse(sessionStorage.getItem('accessToken')));
                    return (<WrappedComponent {...this.props} ability={ability}/>);

                } else {
                    if (this.coreConf.authEnabled) {
                        //auth enabled à true et user pas authentifié, redirection vers le point d'entrée de l'application
                        if (this.coreAuth.path !== window.location.pathname) {
                            return <Switch><Redirect to={this.coreAuth.path} /></Switch>;
                        } else {
                            return null;
                        }
                    //pas d'auth (authEnabled à false) mais le composant semble déclaré avec un withAuth. On renvoi le composant sans rien faire d'autre.
                    } else {
                        return (<WrappedComponent {...this.props}/>);
                    }

                }
            }
        }
    };
