import React from 'react';
import { Link } from 'react-router-dom';
import './HeaderV2.css';
import {Icon} from "antd";
import pluginRegistry from "../../../PluginRegistry";

export default class Header extends React.Component {
    render() {

        const confCore = pluginRegistry.getConfiguration("Core");


        let homeButton = <Link to={confCore.entryPoint.path}><button className="nav-menu-button button-header" ><Icon type={confCore.entryPoint.icon}></Icon></button></Link>;

        return (
            <header>
                <div>
                    <div>
                        <Link to="/home" style={{color: "#fff", fontSize: "1.8em", fontWeight: "bold", letterSpacing: "3px", textDecoration: "none"}}>React-mini-fk</Link>
                        {homeButton}
                    </div>
                </div>
            </header>
        );
    }
}