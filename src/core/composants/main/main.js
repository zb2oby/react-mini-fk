import React from 'react';
import Header from './header/HeaderV2';
import Footer from './footer/Footer';
import Routes from '../../routes/routesV2';
import './main.css';

export default class Main extends React.Component {
    render() {
        return (
            <div className="container-fluide" >
                <Header />
                <div className="menu-component-wrapper" >
                    <div className="page-component-wrapper" >
                        <Routes />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }

}
