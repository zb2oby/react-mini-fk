import React from 'react';
import Rectangle from './Rectangle';
import './Footer.css';

export default class Footer extends React.Component {
    render() {
        return (
            <footer>
                <Rectangle 
                    color="black" 
                    width="auto" 
                    text={`React-mini-fk V1.0.0 - © 2019 Sittron - Tous droits réservés`} />
            </footer>
        );
    }
}