import React from 'react';
import './Rectangle.css';


export default class Rectangle extends React.Component {
    
    render() {
        var style = {};

        if(this.props.color === "black") {
            style = {
                backgroundColor: "#31374A"
            };
        } else {
            style = {
                backgroundColor: "#EF7D24"
            };
        }

        if(this.props.size === "big") {
            style.fontSize = "35px";
            style.lineHeight = "40px";
            style.width = "500px";
        } else if (this.props.size === "medium") {
            style.fontSize = "28px";
            style.lineHeight = "36px";
            style.width = "400px";
        } else {
            style.fontSize = "16px";
            style.lineHeight = "30px";
            style.width = "200px";
        }

        if(this.props.width) {
            style.width = this.props.width;
        }

        if(this.props.height) {
            style.height = this.props.height;
        }

        if(this.props.maxWidth) {
            style.maxWidth = this.props.maxWidth;
        }
        
        return (    
            <div className="rectangle" style={style}>
                <p>{this.props.text}</p>
            </div>
        );
    }
}

Rectangle.defaultProps = {
    color: "orange",
    size: "small",
    text: "insert text"
};