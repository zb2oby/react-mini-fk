import React from "react";
import PluginModuleCard from "../../layouts/pluginsLayouts/PluginModuleCard";
import pluginsRegistry from "../../../PluginRegistry";
import './Home.css';

/**
 * Menu principal de l'application
 * génère les tuiles dynamiquement depuis le registre de plugin en fonction des plugins actifs et droits d'accès
 */
export default class Home extends React.Component {
    render() {
        console.log()
        const authorizedModules = pluginsRegistry.modules.filter((module) => {
            if (((!module.accessControlAction && !module.accessControlEntity) || this.props.ability.can(module.accessControlAction, module.accessControlEntity)) && module.active && module.key !== "Core") {
                return true;
            } else {
                return false;
            }
        });

        const moduleCards = authorizedModules.map((module, index)=> {
            let colorClass = index%2 === 0 ? "colored" : "non-colored";
            return (<PluginModuleCard
                        key={module.key}
                        title={module.title}
                        description={module.description}
                        link={module.entryPoint.path}
                        icon={module.icon}
                        style={{minWidth: 280, textAlign: "center"}}
                        className={`${colorClass}`}
                    />);
        });

        let style = {
            maxWidth: 967,
            margin: "15px 0"
        };

        let classFlex = "justify-content-start";

        if (moduleCards.length <= 2) {
            style.margin = "60px 0";
            style.padding = "30px";
            classFlex = "justify-content-around";
        }

        return (
            <div>
                <div style={style} className={"mx-auto portal-home-container"}>
                    <div className={`p-3 widget-wrapper mx-auto d-flex flex-row ${classFlex} align-items-center flex-wrap`}>
                        {moduleCards}
                    </div>
                </div>
                <div style={{textAlign: "center", marginTop: 40, fontSize: "2rem", color: "#e7c62c"}}>
                    <a style={{color: "#e7c62c"}} href={"https://react-mini-fk-doc.zb2oby.fr"} target={"_blank"}>Voir la documentation</a>
                </div>
            </div>

        )
    }
}