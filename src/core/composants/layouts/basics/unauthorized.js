import React from 'react';
import './auth.css';

/**l
 * Composant d'affichage de la page 401/403
 */
export default class Unauthorized extends React.Component {
    render() {
        return (
            <div style={this.props.style} className={this.props.className + " unauth-container"}>
                {this.props.children}
                <div className={"unauth-section unauth-text"}><p>{this.props.principalMessage ? this.props.principalMessage : "Vous n'êtes pas autorisé(e) à accéder à ce contenu..."}</p></div>
                {this.props.message && <div className={"unauth-text"}>{this.props.message}</div>}
            </div>
        )
    }
}