import React from 'react';
import {Icon} from "antd";


export default class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.enableMessage = this.enableMessage.bind(this);

        this.state = {
            displaySpinner: false,
        };

        this.timer = setTimeout(this.enableMessage, 250);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    enableMessage() {
        this.setState({displaySpinner: true});
    }


    render() {


        const maskStyle = {
            ...this.props.style,
            position: "fixed",
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
            backgroundColor: "rgba(0, 0, 0, 0.35)",
            height: "100%",
            zIndex: "1000",
            textAlign:"center",
            paddingTop: "200px"
        }

        const spinnerStyle = {
            ...this.props.spinnerStyle,
            color: "#ff9f37",
            fontSize: "150px"
        }


        const {displaySpinner} = this.state;

        if (!displaySpinner) {
            return null;
        }

        return (
            <div style={maskStyle} className={this.props.className ? this.props.className + " ant-modal-mask" : "ant-modal-mask"} >
                <Icon type={this.props.icon ? this.props.icon : "loading"} style={spinnerStyle} />
                <p style={{color: "#fff", fontSize: "20px", padding:"15px"}}>{this.props.message ? this.props.message : 'Données en cours de chargement, merci de patienter...'}</p>
            </div>
        )
    }
}