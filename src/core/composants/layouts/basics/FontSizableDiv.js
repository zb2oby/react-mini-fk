import React from 'react';

export default class FontSizableDiv extends React.Component {
    render() {

        const style = {
            ...this.props.style,
            padding: "25px",
            textAlign: "center",
            marginTop: "40px",
            fontSize: this.props.size ? this.props.size === "small" ? "0.8rem" : this.props.size === "medium" ? "1.5rem" : "2rem" : "1rem",
            color: "rgba(0, 0, 0, 0.65)",
        }

        return (
            <div key={"nodata"} style={style} {...this.props}>{this.props.children}</div>
        )
    }
}