import React from 'react';
import './lsp.css';

/**
 * Composant d'affichage d'une page à deux section : une de selection l'autre d'affichage des donnée selectionnées
 */
export default class LateralSelectionPanel extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            itemSelected: null,
        }
    }

    selectItem = (item) => {
        if (this.props.onSelect) {
            this.props.onSelect(item);
        }
        this.setState({itemSelected: item})
    }

    render() {

        const DataPanelElement = this.props.dataComponent

        let selector = this.props.items.map((item)=> {
            let classActive = "";
            if (this.state.itemSelected!== null && this.state.itemSelected[this.props.filterName] === item[this.props.filterName]) {
                classActive = "item-selected";
            }
            return <h4 className={`lsp-selector ${classActive}`} key={item[this.props.filterName]} onClick={() => this.selectItem(item)}>{item[this.props.titleProperty]}</h4>
        })

        let dataByItem = null;
        if (this.state.itemSelected !== null) {
            dataByItem = <DataPanelElement onUpdate={this.props.onUpdate ? this.props.onUpdate : null} item={this.state.itemSelected}/>
        }

        return(
            <div className={"row lateral-component"}>
                <div className={"lateral-section col-lg-3"}>
                    <div className={"left-selector"}>
                        <h2>{this.props.selectorName}</h2>
                        {selector}

                    </div>
                </div>
                <div className={"lateral-section col-lg-9"}>
                    <h2>{this.props.dataSectionName}</h2>
                    {this.state.itemSelected !== null ? dataByItem : <p style={{marginTop:50}} className={"no-selected"}>Aucun élément séléctionné</p>}
                </div>
            </div>
        )
    }
}