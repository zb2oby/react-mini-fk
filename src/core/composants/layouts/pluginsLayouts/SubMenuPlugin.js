import React from 'react';
import {Icon} from "antd";
import HeaderPluginEntryPoint from "./HeaderPluginEntryPoint";
import './pluginsLayouts.css';
import {CreateLinkFromPlugin} from './CreateLinkFromPlugin';

/**
 * Composant de menu d'accès aux différents modules d'un plugin
 * @param props
 */
export default class SubMenuPlugin extends React.Component {
    render() {
        let modules = this.props.plugin.mods;
        if (this.props.externalModules && this.props.externalModules !== null) {
            this.props.externalModules.map((extmod)=> {
                if (!modules.find((mod)=> {return extmod.key === mod.key})){
                    modules.push(extmod);
                }
                return extmod
            })
        }

        let menuItems = modules.map((module)=> {
            if (module.isOnMenu) {
                return (
                    <CreateLinkFromPlugin
                        key={module.key}
                        {...this.props}
                        className={"plugin-submenu-lien"}
                        eltkey={module.key}
                        state={module.state ? module.state : null}
                        objectIdsToLink={this.props.match.params.id ? {id : this.props.match.params.id} : null}
                    >
                        <Icon title={module.title} type={module.icon} className="plugin-submenu-menicon"/>
                        <h1 className="plugin-submenu-h1 plugin-submenu-title">{module.title}</h1>
                    </CreateLinkFromPlugin>

                )
            } else {
                return null;
            }

        });
        return (
            <div className="container-fluide plugin-submenu-main">
                {this.props.header &&
                <HeaderPluginEntryPoint {...this.props}/>}
                <div className="plugin-submenu-icon-menu row">
                    {menuItems}
                </div>
            </div>

            )
    }
}