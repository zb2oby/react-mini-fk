import React from 'react';
import {Icon} from "antd";
import "./pluginsLayouts.css";

/**
 * Composant de header pour les vue de point d'entrée des plugin (le plus souvent des menus)
 * @param props
 */
export default class HeaderPluginEntryPoint extends React.Component {
    render() {
        return (
            <div className="row" >
                <div className="col-md-12" >
                    <h3 className="header-entry-point-title" ><Icon type={this.props.plugin.icon} /> {this.props.plugin.entryPoint.title}</h3>
                    <hr className="header-entry-point-separation-line" />
                </div>
            </div>
        )
    }
}