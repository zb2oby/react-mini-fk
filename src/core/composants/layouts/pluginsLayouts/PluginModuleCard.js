import React from 'react';
import {Card, Icon} from 'antd';
import { Link } from 'react-router-dom';

const {Meta} = Card;

/**
 * Composant d'une tuile du menu principal
 */
export default class ModuleCardV2 extends React.Component {
    render() {
        return (
            <Link className={this.props.className ? this.props.className : ""} style={this.props.style ? {...this.props.style, display: 'block'} : {display: "block"}} to={this.props.link}>
                <Card
                    hoverable
                    cover={<Icon style={{fontSize: "5rem", padding:15, color:"#e7c62c"}} type={this.props.icon}></Icon>}
                >
                    <Meta
                        title={this.props.title}
                        description={this.props.description}
                    />
                </Card>
            </Link>

        )
    }
}