import {Link} from "react-router-dom";
import React from "react";

/**
 * @param props
 * @returns {*}
 * @constructor
 */
export const CreateLinkFromPlugin = (props) => {

    let module = props.plugin.mods.find((module)=> module.key === props.eltkey);

    let linkPath = module.path;
    let accessObject = null;


    if (props.objectIdsToLink && props.objectIdsToLink !== null) {
        if (props.controlAccess === true) {
            accessObject = props.objectIdsToLink;
        }
        let object = props.objectIdsToLink;
        for (let property in object) {
            if (object.hasOwnProperty(property)) {
                linkPath = linkPath.replace(":"+property, props.objectIdsToLink[property]);
            }
        }
    }



    const linkTo = props.state && props.state !== null ? {pathname: linkPath, state: props.state} : linkPath;

    const link =
        <Link className={props.className ? props.className : ""} to={linkTo}>
            {props.children}
        </Link>;


    //si on a des config de vérification de droits
    if (module.accessControlAction && module.accessControlEntity) {
        let entity = module.accessControlEntity;
        //si l'objet à atester n'est pas null on le passe comme entité pour la logique de vérification des habilitations
        if (accessObject !== null) {
            entity = accessObject;
        }

        //on vérifie les droit et en fonction on affiche
        if (props.ability && props.ability.can(module.accessControlAction , entity)) {
            return (
                link
            )
        } else {
            return null
        }

    } else {
        return link
    }
}