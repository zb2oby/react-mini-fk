import React from 'react';
import {Link} from "react-router-dom";

/**
 * Composant de header pour les pages de modules de plugin
 */
export default class HeaderPluginModule extends React.Component {
    render() {
        return(
            <React.Fragment>
                <Link to={this.props.plugin.entryPoint.path}>
                    <button className="back-button">
                        <i className="fas fa-arrow-left" />
                    </button>
                </Link>
                <h1 className="header-plugin-module-h1" > {this.props.module.title} </h1>
            </React.Fragment>
        )
    }
}