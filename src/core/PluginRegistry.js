import {EventEmitter} from 'events';
import Dispatcher from "./Dispatcher";

/**
 * Registre des configurations de plugins
**/
class PluginsRegistry extends EventEmitter{
    constructor(){
        super();
        this.modules = [];
    }

    handleActions(action){
        switch(action.type){
            case "UPDATE_REGISTERED_PLUGINS":
                this.modules = action.modules;
                console.log("UPDATE_PLUGIN", this.modules)
                break;
            case "UPDATE_PLUGIN":
                console.log("UPDATE_PLUGIN", action.plugin)
                let plugins  = this.modules.map((module)=> {
                    if (module.key === action.plugin.key) {
                        return {...module, active: action.plugin.active, title: action.plugin.title, position: action.plugin.position}
                    }else {
                        return module;
                    }
                }).sort((a, b)=> a.position - b.position);
                this.modules = plugins;
                this.emit("plugin_updated");
                console.log(this.modules);
                break;
            default:
        }
    }

    /**
     * Permet d'obtenir la configuration d'un plugin avec ses modules et imports chargés
     * @param key
     * @returns {*}
     */
    getConfiguration(key) {
        return this.modules.find((module) => {return module.key === key});
    }

    /**
     * Permet d'obtenir la configuration complète d'un module avec ses imports chargés
     * @param plugin
     * @param modKey
     * @returns {*}
     */
    getModuleConfiguration(plugin, modKey) {
        let pluginConf = this.getConfiguration(plugin);
        return pluginConf.mods.find((mod) => { return mod.key === modKey});
    }

    /**
     * Permet d'accéder à un composant importé en dépendance. évite les imports direct de fichiers hors module
     * @param plugin
     * @param module
     * @param component
     * @returns {*}
     */
    getDependencieComponent(plugin, module, component) {
        let moduleConf = this.getModuleConfiguration(plugin, module);
        let dependencies = moduleConf.dependencies;
        //console.log(moduleConf)
        return dependencies.find((dep) => {return dep.componentName === component}).component;
    }

}

const pluginRegistry = new PluginsRegistry();
pluginRegistry.setMaxListeners(500);
Dispatcher.register(pluginRegistry.handleActions.bind(pluginRegistry));

export default pluginRegistry;
