import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Main from './core/composants/main/main';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'react-bootstrap-toggle/dist/bootstrap2-toggle.css';
import 'react-bootstrap-toggle/dist/react-bootstrap2-toggle.js';
import 'jquery/dist/jquery';
import './index.css';
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import './assets/fontawesome_free_5_0_13/web-fonts-with-css/css/fontawesome-all.min.css';
import './assets/font-awesome_4_7_0/css/font-awesome.min.css';

import registerServiceWorker from './core/registerServiceWorker';
import {registerPlugins} from "./core/pluginRegistration";

registerPlugins().then(()=> {

ReactDOM.render(
        <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || ''}>
            <Main />
        </BrowserRouter>,
    document.getElementById('root')
);


registerServiceWorker();

});




